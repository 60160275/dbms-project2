import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


require('./bootstrap');


window.Vue = require('vue');
window.axios = require('axios');

Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)


Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('activitys', require('./components/Activitys.vue').default);
Vue.component('create', require('./components/Create.vue').default);
Vue.component('show', require('./components/Show.vue').default);
Vue.component('edit', require('./components/Edit.vue').default);


const app = new Vue({
    el: '#app',
});
