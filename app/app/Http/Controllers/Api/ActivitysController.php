<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ActivitysModel;
class ActivitysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activitys=ActivitysModel::all();
        return response()->json($activitys);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $activitys=new ActivitysModel();
        $activitys->activityName=$request->get('activityName');
        $activitys->activityDateStart=$request->get('activityDateStart');
        $activitys->activityDateEnd=$request->get('activityDateEnd');
        $activitys->activityAmount=$request->get('activityAmount');
        $activitys->activityLocation=$request->get('activityLocation');
        $activitys->save();
        return response()->json($activitys);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activitys=ActivitysModel::find($id);
        return response()->json($activitys);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $activity=new ActivitysModel();
        $activity->activityName=$request->get('activityName');
        $activity->activityDateStart=$request->get('activityDateStart');
        $activity->activityDateEnd=$request->get('activityDateEnd');
        $activity->activityAmount=$request->get('activityAmount');
        $activity->activityLocation=$request->get('activityLocation');
        $activity->update();
        return response()->json($activity);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity=ActivitysModel::find($id);
        $activity->delete();
        return response()->json($activity);
    }
}
