<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivitysModel extends Model
{
    protected $table="Actvitys";
    protected $fillable = [
        'activityName',
        'activityDateStart',
        'activityDateEnd',
        'activityAmount',
        'activityLocation'
        
    ];
}
