<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Actvitys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('activityName');
            $table->date('activityDateStart');
            $table->date('activityDateEnd');
            $table->integer('activityAmount');
            $table->string('activityLocation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Actvitys');
    }
}
